﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.DTO.Character;
using MovieAPI.Services;

namespace MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class CharactersController : Controller
    {
        private readonly ICharacterService _service;
        private readonly IMapper _mapper;
        public CharactersController(ICharacterService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all characters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _service.getAllCharactersAsync());
        }

        /// <summary>
        /// Get a specific character
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterById(int id)
        {

            Character character = await _service.getCharacterByIdAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Create a new character
        /// </summary>
        /// <param name="dtoCharacter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Character>> AddCharacter(CharacterReadDTO dtoCharacter)
        {
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            domainCharacter = await _service.AddCharacterAsync(domainCharacter);
            return CreatedAtAction("GetCharacterById",
                new { id = domainCharacter.Id },
                _mapper.Map<CharacterReadDTO>(domainCharacter));
        }

        /// <summary>
        /// Edit a character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoCharacter"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCharacter(int id, CharacterEditDTO dtoCharacter)
        {

            if (id != dtoCharacter.Id)
            {
                return BadRequest();
            }

            if (!_service.CharacterExist(id))
            {
                return NotFound();
            }
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            await _service.UpdateCharacterAsync(domainCharacter);

            return NoContent();
        }

    }
}