﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.DTO.Character;
using MovieAPI.Models.DTO.Franchise;
using MovieAPI.Models.DTO.Movie;
using MovieAPI.Services;

namespace MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class FranchisesController : ControllerBase
    {
   
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;
        private readonly IMovieService _movieService;

        public FranchisesController(IMapper mapper, IFranchiseService franchiseService, IMovieService movieService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
            _movieService = movieService; 
        }

        /// <summary>
        /// Gets all movies from database
        /// </summary>
        /// 
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.getAllFranchiseAsync());
        }


        /// <summary>
        /// Gets a specific franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {
     
            Franchise franchise = await _franchiseService.getFranchiseByIdAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }
        /// <summary>
        /// Get all movies based on franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> getMoviesByFranchise(int id)
        {
            List<MovieReadDTO> movies = new();

            var dtoFranchise = _mapper.Map<FranchiseReadDTO>(await _franchiseService.getFranchiseByIdAsync(id)); 
            if (dtoFranchise == null)
            {
                return NotFound();
            }

            foreach(int movie in dtoFranchise.Movies)
            {
                var movieMatch = _mapper.Map<MovieReadDTO>(await _movieService.getMovieByIdAsync(movie));
                movies.Add(movieMatch);
            }
            return movies; 
        }


        /// <summary>
        /// Create a new franchise
        /// </summary>
        /// <param name="dtoFranchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> CreateFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchiseById",
                new { id = domainFranchise.Id },
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }


        /// <summary>
        /// Edit a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoFranchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditFranchise(int id, FranchiseEditDTO dtoFranchise)
        {
            if(id != dtoFranchise.Id)
            {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExist(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);

            await _franchiseService.UpdateFranchiseAsync(domainFranchise);

            return NoContent();
        }


        /// <summary>
        /// Delete a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {

            var franshise = await _franchiseService.getFranchiseByIdAsync(id);
            if (!_franchiseService.FranchiseExist(id))
            {
                return NotFound();
            }
           
            if (franshise == null)
            {
                return NotFound();
            } 
            franshise.Movies.Select(m => m.FranchiseId = null);

            await _franchiseService.DeleteFranchiseAsync(id);
            return NoContent();
        }


        /// <summary>
        /// Add movie to a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="moviesList"></param>
        /// <returns></returns>
        [HttpPost("movieFranchise/{id}")]
        public async Task<ActionResult> AssignNewMovieToFranchise(int id, [FromBody] List<int> moviesList)
        {
            Franchise franchise = await _franchiseService.getFranchiseByIdAsync(id);

            if(franchise == null)
            {
                return NotFound();
            }



            foreach(var mov in moviesList)
            {
                var movie = await _movieService.getMovieByIdAsync(mov);

                franchise.Movies.Add(movie);
            }


            await _franchiseService.UpdateFranchiseAsync(franchise);

            return NoContent();
        }
        
    }
}
