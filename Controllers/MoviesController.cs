﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.DTO.Character;
using MovieAPI.Models.DTO.Movie;
using MovieAPI.Services;

namespace MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class MoviesController : ControllerBase
    {


        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;
        private readonly ICharacterService _characterService;

        public MoviesController(MovieDbContext context, IMapper mapper, IMovieService movieService, ICharacterService characterService)
        {
            _context = context;
            _mapper = mapper;
            _movieService = movieService;
            _characterService = characterService; 
        }
        

           /// <summary>
           /// Get all movies
           /// </summary>
           /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {

            return _mapper.Map<List<MovieReadDTO>>(await _context.Movies
                .Include(m => m.Characters)
                .ToListAsync());
            
        }


        /// <summary>
        /// Get a specific movie 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {

            Movie movie = await _movieService.getMovieByIdAsync(id);

            if (movie == null)
            {
                return NotFound();
            }
            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Get all characters in a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> getCharactersInMovie(int id)
        {
            List<CharacterReadDTO> characters = new();

            var dtoMovie = _mapper.Map<MovieReadDTO>(await _movieService.getMovieByIdAsync(id));
            if (dtoMovie == null)
            {
                return NotFound();
            }

            foreach (int character in dtoMovie.Characters)
            {
                var characterMatch = _mapper.Map<CharacterReadDTO>(await _characterService.getCharacterByIdAsync(character));
                characters.Add(characterMatch);
            }
            return characters;
        }

        /// <summary>
        /// Create a new movie
        /// </summary>
        /// <param name="dtoMovie"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> CreateMovie(MovieCreateDTO dtoMovie)
        {
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);

            domainMovie = await _movieService.AddMovieAsync(domainMovie);

            return CreatedAtAction("GetMovie",
                new { id = domainMovie.Id },
                _mapper.Map<MovieReadDTO>(domainMovie));
        }




        /// <summary>
        /// Edit a movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoMovie"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditMovie(int id, MovieEditDTO dtoMovie)
        {

            if (id != dtoMovie.Id)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExist(id))
            {
                return NotFound();
            }


            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);

            await _movieService.UpdateMovieAsync(domainMovie);

            return NoContent();
        }

        
        /// <summary>
        /// Delete a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {

            if (!_movieService.MovieExist(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Assign a new characters to a movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characherList"></param>
        /// <returns></returns>
        [HttpPost("characterMovie/{id}")]
        public async Task<ActionResult> AssignNewCharacterToMovie(int id, [FromBody] List<int> characherList)
        {
            Movie movie = await _movieService.getMovieByIdAsync(id);

            if (movie == null)
            {
                return NotFound();
            }



            foreach (var chara in characherList)
            {
                var character = await _characterService.getCharacterByIdAsync(chara);

                movie.Characters.Add(character);
            }


            await _movieService.UpdateMovieAsync(movie);

            return NoContent();
        }
    }
}
