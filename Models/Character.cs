﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Models
{
    [Table("Characters")]
    public class Character
    {
        [Required]
        public int Id{ get; set; }
        [Required]
        [MaxLength(70)]
        public string FirstName { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender{ get; set; }
        [Url]
        public string Picture { get; set; }

        public ICollection<Movie> AppearsIn { get; set; }
    }
}
