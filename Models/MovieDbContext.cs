﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Models
{
    public class MovieDbContext:DbContext
    {
        public MovieDbContext( DbContextOptions options) : base(options)
        {
        }

        public DbSet<Character> Characters{ get; set; }
        public DbSet<Movie> Movies{ get; set; }
        public DbSet<Franchise> Franchises { get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>().HasData(new Franchise {Id = 1, Name = "Star Wars", Description= "The story of the original trilogy focuses on Luke Skywalker's quest to become a Jedi, his struggle with the evil Imperial agent Darth Vader, and the struggle of the Rebel Alliance to free the galaxy from the clutches of the Galactic Empire." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "Lord of the Rings", Description = "The Lord of the Rings is the saga of a group of sometimes reluctant heroes who set forth to save their world from consummate evil. Its many worlds and creatures were drawn from Tolkien's extensive knowledge of philology and folklore." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 3, Name = "Shrek", Description = "The series primarily focuses on Shrek, a bad-tempered but good-hearted ogre, who begrudgingly accepts a quest to rescue a princess, resulting in him finding friends and going on many subsequent adventures in a fairy tale world." });

            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 1, FranchiseId = 1, Title = "Star Wars: Return of the Jedi", Director = "George Lucas", Genre = "Science Fiction, Fantasy, Space Opera", ReleaseYear ="1983", Trailer = "https://www.youtube.com/watch?v=7L8p7_SLzvU", Picture = "https://upload.wikimedia.org/wikipedia/en/thumb/b/b2/ReturnOfTheJediPoster1983.jpg/220px-ReturnOfTheJediPoster1983.jpg",  });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 2, FranchiseId = 1, Title = "Star Wars: A new Hope", Director = "George Lucas", Genre = "Science Fiction, Fantasy, Space Opera", ReleaseYear = "1977", Picture = "https://en.wikipedia.org/wiki/Star_Wars_(film)#/media/File:StarWarsMoviePoster1977.jpg", Trailer = "https://www.youtube.com/watch?v=1g3_CFmnU7k" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 3, FranchiseId = 2, Title = "The Lord of the Rings: The Two Towers", Director = "Peter Jackson", Genre = "Fantasy", ReleaseYear = "2002", Picture = "https://upload.wikimedia.org/wikipedia/en/d/d0/Lord_of_the_Rings_-_The_Two_Towers_%282002%29.jpg", Trailer = "https://www.youtube.com/watch?v=LbfMDwc4azU" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 4, FranchiseId = 2, Title = "The Lord of the Rings: Return of the King", Director = "Peter Jackson", Genre = "Fantasy", ReleaseYear = "2003", Picture = "https://upload.wikimedia.org/wikipedia/en/b/be/The_Lord_of_the_Rings_-_The_Return_of_the_King_%282003%29.jpg", Trailer = "https://www.youtube.com/watch?v=r5X-hFf6Bwo"});
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 5, FranchiseId = 3, Title = "Shrek", Director = "Andrew Adamson", Genre ="Animation", ReleaseYear = "2001", Picture = "https://en.wikipedia.org/wiki/Shrek#/media/File:Shrek_(2001_animated_feature_film).jpg", Trailer = "https://www.youtube.com/watch?v=CwXOrWvPBPk"});
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 6, FranchiseId = 3, Title = "Shrek 2", Director = "Andrew Adamson", Genre ="Animation", ReleaseYear = "2004", Picture = "https://en.wikipedia.org/wiki/File:Shrek_2_poster.jpg", Trailer = "https://www.youtube.com/watch?v=V6X5ti4YlG8" });

            modelBuilder.Entity<Character>().HasData(new Character { Id = 1, FirstName = "Luke", Alias = "Red-5", Gender = "Male",Picture = "https://en.wikipedia.org/wiki/Luke_Skywalker#/media/File:Luke_Skywalker.png"});
            modelBuilder.Entity<Character>().HasData(new Character { Id = 3, FirstName = "Frodo", Alias = "Ringbearer", Gender = "Male",Picture = "https://upload.wikimedia.org/wikipedia/en/thumb/4/4e/Elijah_Wood_as_Frodo_Baggins.png/170px-Elijah_Wood_as_Frodo_Baggins.png" });
            modelBuilder.Entity<Character>().HasData(new Character { Id = 4, FirstName = "Sauron", Alias = "The dark lord", Gender = "Male", Picture = "https://upload.wikimedia.org/wikipedia/en/thumb/3/3a/Sauron.jpg/290px-Sauron.jpg" }); 
            modelBuilder.Entity<Character>().HasData(new Character { Id = 5, FirstName = "Shrek", Alias = "Ogre", Gender = "Male", Picture = "https://upload.wikimedia.org/wikipedia/en/thumb/9/99/ShrekHuman.png/160px-ShrekHuman.png" }); 
            modelBuilder.Entity<Character>().HasData(new Character { Id = 6, FirstName = "Donkey", Alias = "Stallion", Gender = "Male", Picture = "https://upload.wikimedia.org/wikipedia/en/thumb/6/6c/Donkey_%28Shrek%29.png/130px-Donkey_%28Shrek%29.png" }); 
            modelBuilder.Entity<Character>().HasData(new Character { Id = 2, FirstName = "Storm Trooper With Binoculars", Gender = "Male", });

            // Seed m2m coach-certification. Need to define m2m and access linking table
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.AppearsIn)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 },
                            new { MovieId = 2, CharacterId = 1 },
                            new { MovieId = 3, CharacterId = 3 },
                            new { MovieId = 3, CharacterId = 4 },
                            new { MovieId = 4, CharacterId = 3 },
                            new { MovieId = 4, CharacterId = 4 },
                            new { MovieId = 5, CharacterId = 5 },
                            new { MovieId = 5, CharacterId = 6 },
                            new { MovieId = 6, CharacterId = 5 },
                            new { MovieId = 6, CharacterId = 6 }
                        );
                    });

        }

    }
}

