﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace MovieAPI.Models
{
    [Table("Movie")]
    public class Movie
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(200)]
        public string Genre { get; set; }
        public string ReleaseYear { get; set; }
        [Required]
        [MaxLength(200)]
        public string Director { get; set; }
        [Url]
        public string Picture { get; set; }
        [Url]
        public string Trailer { get; set; }

        public Franchise Franchise { get; set; }
        public int? FranchiseId { get; set; }

        public ICollection<Character> Characters { get; set; }

    }
}
