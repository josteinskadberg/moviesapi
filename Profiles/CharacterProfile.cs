﻿using MovieAPI.Models;
using MovieAPI.Models.DTO.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MovieAPI.Models.DTO.Character;

namespace MovieAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {

            CreateMap<Character, CharacterReadDTO>()
                    .ForMember(mdto => mdto.AppearsIn, opt => opt
                    .MapFrom(m => m.AppearsIn.Select(m => m.Id).ToArray()))
                    .ReverseMap();
            CreateMap<CharacterEditDTO, Character>();
            CreateMap<CharacterCreateDTO, Character>(); 
        }
    }
}
