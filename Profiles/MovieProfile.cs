﻿using MovieAPI.Models;
using MovieAPI.Models.DTO.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;



namespace MovieAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(m => m.Id).ToArray()))
                .ReverseMap();
            CreateMap<MovieEditDTO, Movie>();
            CreateMap<MovieCreateDTO, Movie>(); 
        }
    }
}
