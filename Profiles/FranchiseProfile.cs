﻿using AutoMapper;
using MovieAPI.Models;
using MovieAPI.Models.DTO.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Profiles
{
    public class FranchiseProfile:Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                    .ForMember(fdto => fdto.Movies, opt => opt
                    .MapFrom(m => m.Movies.Select(m => m.Id).ToArray()))
                    .ReverseMap();
            CreateMap<FranchiseEditDTO, Franchise>();
            CreateMap<FranchiseCreateDTO, Franchise>();
        }

    }
}
