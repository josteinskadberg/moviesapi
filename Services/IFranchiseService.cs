﻿using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> getAllFranchiseAsync();
        public Task<Franchise> getFranchiseByIdAsync(int id);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExist(int id);
    }
}
