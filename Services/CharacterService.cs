﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    public class CharacterService : ICharacterService
    {
        private readonly MovieDbContext _context; 


        public CharacterService(MovieDbContext context)
        {
            _context = context; 
        }

        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        public bool CharacterExist(int id)
        {
            return _context.Characters.Any(c => c.Id == id);
        }

        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

        }

        public async Task<IEnumerable<Character>> getAllCharactersAsync()
        {
            return await _context.Characters
                .Include(c => c.AppearsIn)
                .ToListAsync();

        }

        public async Task<Character> getCharacterByIdAsync(int id)
        {
            return await _context.Characters.Include(c => c.AppearsIn).Where(m => m.Id == id).FirstOrDefaultAsync(); ;
        }

        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync(); 

        }
    }
}
