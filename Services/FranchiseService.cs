﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    public class FranchiseService : IFranchiseService
    {

        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExist(int id)
        {
            return _context.Franchises.Any(f => f.Id == id);
        }

        public async Task<IEnumerable<Franchise>> getAllFranchiseAsync()
        {
            return await _context.Franchises
                .Include(c => c.Movies)
                .ToListAsync();
        }

        public async Task<Franchise> getFranchiseByIdAsync(int id)
        {
            return await _context.Franchises.Include(f => f.Movies).Where(m => m.Id == id).FirstOrDefaultAsync();
        }

        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}


