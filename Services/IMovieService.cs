﻿using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> getAllMoviesAsync();
        public Task<Movie> getMovieByIdAsync(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);
        public Task DeleteMovieAsync(int id);
        public bool MovieExist(int id);
    }
}
