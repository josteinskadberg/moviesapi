﻿using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    public interface ICharacterService
    {
        public Task<IEnumerable<Character>> getAllCharactersAsync();
        public Task<Character> getCharacterByIdAsync(int id);
        public Task<Character> AddCharacterAsync(Character character);
        public Task UpdateCharacterAsync(Character character);
        public Task DeleteCharacterAsync(int id);
        public bool CharacterExist(int id);    
    }
}
