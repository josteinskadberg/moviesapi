﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    

    public class MovieService: IMovieService
    {

        private readonly MovieDbContext _context;

        public  MovieService(MovieDbContext context)
        {
            _context = context; 
        }

        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;  
        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Movie>> getAllMoviesAsync()
        {
            return await _context.Movies
                .Include(c => c.Characters)
                .ToListAsync(); 
        }

        public async Task<Movie> getMovieByIdAsync(int id)
        {
            return await _context.Movies.Include(m => m.Characters).Where(c => c.Id == id).FirstOrDefaultAsync();
        }



        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync(); 
        }

        public bool MovieExist(int id)
        {
            return _context.Movies.Any(f => f.Id == id);
        }
    }
}
